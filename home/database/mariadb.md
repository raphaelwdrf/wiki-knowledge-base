---
title: MariaDB
description: 
published: true
date: 2020-05-29T17:26:14.968Z
tags: 
editor: undefined
---

# MariaDB (Fork of MySQL)
MariaDB is a community-developed, commercially supported fork of the MySQL relational database management system (RDBMS), intended to remain free and open-source software under the GNU General Public License.

> MariaDB is maintained up to date with the latest MySQL version and will work exactly like MySQL. All commands, interfaces, libs and APIs avaiable on MySQL are avaiable on MariaDB. There is no need to convert data to work wirh MariaDB, it is fully compatible.
{.is-info}

*`MariaDB is named after Monty's(Founder) younger daughter, Maria and MySQL is named after his other daughter, My.`*

## Source
https://en.wikipedia.org/wiki/MariaDB


# Query
## Command line running script sql
```shell
mysql -u [username] -p [dbname] < somedb.sql
```
```shell
mysql -u [username] -p [dbname] -e [query]
```
- If authenticated on mysql
```shell
mysql> source get_databases.sql
```
### Source
https://electrictoolbox.com/run-single-mysql-query-command-line/

## Change charset on db, table and collumn
> Pertinent:
> [about-charset-utf8-and-utf8mb4](#about-charset-utf8-and-utf8mb4)
{.is-info}

- Changing single database default charset:
```sql
ALTER DATABASE table_name
  CHARACTER SET = 'utf8mb4'
  COLLATE = 'utf8mb4_unicode_ci';
```
- Changing single table default charset:
```sql
ALTER TABLE table_name
  CHARACTER SET utf8,
  COLLATE utf8_general_ci;
```
- Change all tables default charset:
```sql
SELECT CONCAT('alter table ',TABLE_SCHEMA,'.',TABLE_NAME,' charset=utf8mb4;')
  FROM information_schema.TABLES
  WHERE TABLE_SCHEMA != 'information_schema';
```
- Change all created tables/columns charset:
```sql
SELECT CONCAT('alter table ',TABLE_SCHEMA,'.',TABLE_NAME,' alter column ',COLUMN_NAME,' charset=utf8mb4;')
  FROM information_schema.COLUMNS
  WHERE TABLE_SCHEMA != 'information_schema';
```
### Source
https://stackoverflow.com/questions/8906813/how-to-change-the-default-charset-of-a-mysql-table

# my.ini
## Change charset
```ini
[client]
...
default-character-set=utf8mb4
...
[mysql]
...
default-character-set=utf8mb4
...
[mysqld]
...
collation-server = utf8mb4_unicode_ci
init-connect='SET NAMES utf8mb4'
character-set-server = utf8mb4
...
```
### Source
https://mariadb.com/kb/en/setting-character-sets-and-collations/#example-changing-the-default-character-set-to-utf-8

# Information
## About charset utf8 and utf8mb4
> In MariaDB, the default character set is latin1, and the default collation is latin1_swedish_ci
{.is-info}

On MySQL the “utf8” encoding only supports three bytes per character. The real UTF-8 encoding needs up to four bytes per character. MySQL developers never fixed this bug. They released a workaround in 2010: a new character set called “utf8mb4”.

> - MySQL’s “utf8mb4” means “UTF-8”.
> - MySQL’s “utf8” means “a proprietary character encoding”. This encoding can’t encode many Unicode characters.

> If you need to use MySQL or MariaDB, never use “utf8”. Always use “utf8mb4” when you want UTF-8.
{.is-warning}

### Source
https://medium.com/@adamhooper/in-mysql-never-use-utf8-use-utf8mb4-11761243e434

# MySQL
## Increase max_connections
File location`/etc/mysql/my.cnf`
Insert below `[mysqld]` as below:
```ini
[mysqld]
max_connections=1000
```