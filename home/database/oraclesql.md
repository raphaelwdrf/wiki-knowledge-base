---
title: OracleSQL
description: 
published: true
date: 2020-05-19T13:34:08.783Z
tags: 
editor: undefined
---

# Oracle SQL
None

# Query
## Reset expired password
```SQL
--Acessar o "Executar linhas de comandos SQL" ou 'sqlplus' via terminal ou abrir diretamente o executável

SLQ> connect sys as sysdba 
--Senha checar keepass

--Mostra todos os usuários do banco de dados e os respectivos status 
SELECT username, account_status, expiry_date FROM dba_users;

--Desbloquear a senha, quando se conhece o usuário
ALTER USER user_name IDENTIFIED BY password ACCOUNT UNLOCK;

--Retira o tempo de expiração de senha
ALTER PROFILE <profile_name> LIMIT PASSWORD_LIFE_TIME UNLIMITED;

--Trocar a senha
ALTER USER user_name IDENTIFIED BY password DEFAULT new_password;
```

Caso não tenha acesso ao usuário sys:
```shell
cd {directoryBinOracle}
# Acessar o "Executar linhas de comandos SQL" ou 'sqlplus' via terminal ou abrir diretamente o executável
sqlplus
# Será solicitado login e senha, utilizar o do usuário expirado
# Será solicitado que altere a senha, utilizar a mesma senha
```
```SQL
--Retira o tempo de expiração de senha
ALTER PROFILE <profile_name> LIMIT PASSWORD_LIFE_TIME UNLIMITED;
```