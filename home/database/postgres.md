---
title: Postgres
description: 
published: true
date: 2020-12-08T13:11:20.941Z
tags: 
editor: undefined
---

# Postgres
None

# Query
## Command line running script sql
```powershell
psql -U username -d myDataBase -a -f myInsertFile.sql
```
- Database remote:
```shell
psql -h host -U username -d myDataBase -a -f myInsertFile.sql
```
- Detailed:
```powershell
psql -h localhost -d myDataBase -U username -p 5432 -a -q -f myInsertFile.sql
# -h PostgreSQL server IP address
# -d database name
# -U user name
# -p port which PostgreSQL server is listening on
# -f path to SQL script
# -a all echo
# -q quiet
```

### Source
https://stackoverflow.com/questions/9736085/run-a-postgresql-sql-file-using-command-line-arguments

## Working with dates and times
```sql
TIMESTAMP '1999-12-30' - TIMESTAMP '1999-12-11' = INTERVAL '19 days'
```
```sql
INTERVAL '1 month' + INTERVAL '1 month 3 days' = INTERVAL '2 months 3 days'
```
```sql
TIMESTAMP '2001-07-02' + INTERVAL '1 month' = TIMESTAMP '2001-08-02'
TIMESTAMP '2001-07-02' + INTERVAL '31 days' = TIMESTAMP '2001-08-02'
```

### Source
https://wiki.postgresql.org/wiki/Working_with_Dates_and_Times_in_PostgreSQL

# Non-query
## Sequence
### Increase current value for pkey
- Access database via pgadmin
- Access 'sequence'
- Find pkey that you want to change
- Right-click then propeties
- Edit current value
- Save

#### Source
https://stackoverflow.com/a/33430527

# Tuning
Tuning tools:
https://github.com/darold/pgbadger
https://github.com/jfcoz/postgresqltuner

## Pgtune
https://pgtune.leopard.in.ua/#/
### Usage example
```ini
#------------------------------------------------------------------------------
#ATIVA TUNNING 13-05-2020 PGTUNE.LEOPARD.IN.UA
#------------------------------------------------------------------------------
# DB Version: 10
# OS Type: windows
# DB Type: web
# Total Memory (RAM): 2 GB # On this example we had 8gb SysRAM but we reserved 2GB to TomCat; 2GB Windows; 2GB SC
# CPUs num: 4
# Connections num: 250 # Check on hibernate.conf.xml on SC.jar the value on hibernate.c3p0.max_size
# Data Storage: hdd

max_connections = 250
shared_buffers = 512MB
effective_cache_size = 1536MB
maintenance_work_mem = 128MB
checkpoint_completion_target = 0.7
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 4
work_mem = 1048kB
min_wal_size = 1GB
max_wal_size = 4GB
max_worker_processes = 4
max_parallel_workers_per_gather = 2
max_parallel_workers = 4
```