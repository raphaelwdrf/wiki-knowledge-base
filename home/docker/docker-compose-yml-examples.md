---
title: Docker Compose YML Examples
description: 
published: true
date: 2020-05-18T17:20:23.009Z
tags: 
editor: undefined
---

# Examples
## Mutiples database with config on volume
```yml
version: "2.3"

networks:
  mysql01:
    driver: bridge
  mariadb01:
    driver: bridge
  postgres01:
    driver: bridge
  mongo01:
    driver: bridge
  mssql01:
    driver: bridge
  adminer01:
    driver: bridge
  mongo-express01:
    driver: bridge

services:

#################
#   Databases   #
#################

  mysql01:
    image: mysql:5
    container_name: mysql01
    environment:
      - MYSQL_ROOT_PASSWORD=mysql01
      - MYSQL_DATABASE=sollus
      - MYSQL_USER=sollus
      - MYSQL_PASSWORD=sollus
      - TZ=America/Sao_Paulo
    volumes:
      - ./mysql01-docker/var/lib/mysql:/var/lib/mysql
      - ./docker-compose-ativa-dbs/mysql01/etc/mysql/mysql.cnf:/etc/mysql/mysql.cnf
    restart: unless-stopped
    ports:
      - 3306:3306
    networks:
      - mysql01

  mariadb01:
    image: mariadb:10
    container_name: mariadb01
    environment:
      - MYSQL_ROOT_PASSWORD=mariadb01
      - MYSQL_DATABASE=sollus
      - MYSQL_USER=sollus
      - MYSQL_PASSWORD=sollus
      - TZ=America/Sao_Paulo
    volumes:
      - ./mariadb01-docker/var/lib/mysql:/var/lib/mysql
      - ./docker-compose-ativa-dbs/mariadb01/etc/mysql/my.cnf:/etc/mysql/my.cnf
    restart: unless-stopped
    ports:
      - 3307:3306
    networks:
      - mariadb01

  postgres01:
    image: postgres:10
    container_name: postgres01
    environment:
      - POSTGRES_PASSWORD=postgres01
      - POSTGRES_DB=sollus
      # - POSTGRES_USER=sollus
      - TZ=America/Sao_Paulo
    volumes:
      - ./postgres01-docker/var/lib/postgresql/data:/var/lib/postgresql/data
      - ./docker-compose-ativa-dbs/postgres01/var/lib/postgresql/data/postgresql.conf:/var/lib/postgresql/data/postgresql.conf
    restart: unless-stopped
    ports:
      - 3308:3306
    networks:
      - postgres01

  mongo01:
    image: mongo:4-bionic
    container_name: mongo01
    environment:
      - MONGO_INITDB_ROOT_USERNAME=root
      - MONGO_INITDB_ROOT_PASSWORD=mongo01
      - TZ=America/Sao_Paulo
    volumes:
      - ./mongo01-docker/data/db:/data/db
      - ./docker-compose-ativa-dbs/mongo01/etc/mongo/mongod.conf:/etc/mongo/mongod.conf
    restart: unless-stopped
    ports:
      - 27017:27017
    command: "--config /etc/mongo/mongod.conf"
    networks:
      - mongo01

  mssql01:
  # https://docs.microsoft.com/pt-br/sql/linux/sql-server-linux-configure-docker?view=sql-server-ver15
    image: mcr.microsoft.com/mssql/server:2017-latest
    container_name: mssql01
    environment:
      - ACCEPT_EULA=Y
      - SA_PASSWORD=mssql01_
      - TZ=America/Sao_Paulo
    volumes:
      - ./mssql01-docker/var/opt/mssql:/var/opt/mssql
    restart: unless-stopped
    ports:
      - 1433:1433
    networks:
      - mssql01


#################
#   DB Manag.   #
#################

  adminer01:
  # MySQL, PostgreSQL, SQLite, MS SQL, Oracle, Firebird, SimpleDB, Elasticsearch and MongoDB
    image: adminer
    container_name: adminer01
    environment:
      - ADMINER_DESIGN=pepa-linha
      - TZ=America/Sao_Paulo
    restart: unless-stopped
    ports:
      - 8080:8080
    networks:
      - adminer01
      - mysql01
      - mariadb01
      - postgres01
      - mongo01
      - mssql01

  mongo-express01:
    image: mongo-express
    container_name: mongo-express01
    environment:
      - ME_CONFIG_BASICAUTH_USERNAME=mongoexpress01
      - ME_CONFIG_BASICAUTH_PASSWORD=mongoexpress01
      - ME_CONFIG_MONGODB_SERVER=mongo01
      # - ME_CONFIG_MONGODB_AUTH_DATABASE=db
      - ME_CONFIG_MONGODB_ADMINUSERNAME=root
      - ME_CONFIG_MONGODB_ADMINPASSWORD=mongo01
      - TZ=America/Sao_Paulo
    restart: unless-stopped
    ports:
      - 8081:8081
    networks:
      - mongo-express01
      - mongo01