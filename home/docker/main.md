---
title: Main
description: 
published: true
date: 2020-07-10T19:42:51.936Z
tags: 
editor: undefined
---

# Docker
None

# Docker-daemon

## Cleaning Docker

- Docker-compose clean up

```sh
$ docker-compose down -v --rmi all --remove-orphans
```

### Source
https://vsupalov.com/cleaning-up-after-docker/

## Force container to keep alive
> Always stop the container or docker service to avoid problems.
> Need to run as root (sudo su) to edit config files from docker.
{.is-warning}

- Stop container
```powershell
docker container restart containerID/name
```

- Edit file `/var/lib/docker/containers/923...4f6/config.json` where `923...4f6` correspond to stopped container-ID.

> Before edit save the original text.
{.is-warning}

- Edit Path and Args as below:
`"Path":"tail","Args":["-f","/dev/null"]`

- Restart docker service.
```powershell
systemctl restart docker
```

- Start container.
```powershell
docker container restart containerID/name
```

### Source
https://stackoverflow.com/questions/32353055/how-to-start-a-stopped-docker-container-with-a-different-command

## login on container as root
```shell
docker exec -it --user root $(containerName) sh
```

### Source
https://unix.stackexchange.com/questions/39314/su-does-not-change-user-but-does-not-respond-with-an-error-either

# Docker-compose
https://docs.docker.com/compose/compose-file/compose-file-v2/
> Recommended to call on yaml `version: "2.3"`.
{.is-info}

## Maintain container running
The example below mantain the container running even if no command was defined to run, it resources on sh running forever.

- Example:
```yaml
ubuntu:
  image: ubuntu:latest
  entrypoint: sh
  stdin_open: true
  tty: true
```
### Source
https://stackoverflow.com/questions/36249744/interactive-shell-using-docker-compose

## Healthcheck
- On the dependent container:
```yaml
depends_on:
	service_name:
		condition: service_healthy # alt. service_started
```
### Mongo
```yaml
version: '2.3'
mongo:
	image: mongo:4
# ...
	healthcheck:
		test: echo 'db.runCommand({ping:1}).ok' | mongo localhost:27017/test --quiet | grep 1
		interval: 10s
		timeout: 10s
		retries: 5
		# start_period: 30s
```
#### Source
https://stackoverflow.com/a/62066529
https://github.com/docker-library/healthcheck

### MariaDB
```yaml
  healthcheck:
    test: "mysqladmin -p$$MYSQL_ROOT_PASSWORD ping -h localhost && mysql -p$$MYSQL_ROOT_PASSWORD -e 'select 1'" # PING shows server is running, mysql -e shows if it is accepting connection
    interval: 10s
    timeout: 10s
    retries: 3
    start_period: 10s
```
#### Source
https://stackoverflow.com/a/42757250
https://electrictoolbox.com/run-single-mysql-query-command-line/

### wget
```yml
healthcheck:
	test: "wget -q --spider http://192.168.1.84:7002"
 	interval: 2s
 	timeout: 2s
 	retries: 5
 	start_period: 10s
```

### sh
> Good to learn
> http://blog.evaldojunior.com.br/aulas/blog/shell%20script/2011/05/08/shell-script-parte-2-controle-de-fluxo.html
```yml
healthcheck:
	test: ["CMD-SHELL", "/home/healthcheck.sh"]
	interval: 2s
	timeout: 2s
	retries: 5
	start_period: 10s
```

- Example .sh
```powershell
ifconfig | grep tun0 > /dev/null; if [ 0 != $? ]; then exit 1; fi;
```

## Change timezone
```yaml
ubuntu:
  image: ubuntu:latest
  environment:
    - TZ=America/Sao_Paulo
```
> On **Alpine Linux** container you must download tzdata `apk add tzdata` or follow instructions on [/home/linux/main#set-timezone](/home/linux/main#set-timezone)
{.is-warning}

### Source
https://wiki.alpinelinux.org/wiki/Setting_the_timezone

## Multiple commands
```yaml
command: sh -c "python manage.py migrate && python manage.py runserver 0.0.0.0:8000"
```
```yaml
    command: >
      sh -c "python manage.py wait_for_db &&
             python manage.py migrate &&
             python manage.py runserver 0.0.0.0:8000"
```

### Source
https://stackoverflow.com/questions/30063907/using-docker-compose-how-to-execute-multiple-commands

## Logrotate
- Find or create `/etc/docker/daemon.json`
```json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m",
    "max-file": "3",
    "compress": "true"
  }
}
```

### Source
https://docs.docker.com/config/containers/logging/configure/
https://docs.docker.com/config/containers/logging/json-file/
https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-configuration-file
