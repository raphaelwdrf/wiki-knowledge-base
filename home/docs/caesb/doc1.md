---
title: IT Caesb 001 Limpeza Sensor de Nível
description: 
published: true
date: 2020-08-03T21:24:46.748Z
tags: 
editor: undefined
---

![logo-ativa.png](/images/logo-ativa.png =100x) ![logo-caesb.png](/images/caesb/logo-caesb.png =100x)
# IT Caesb 001 Limpeza Sensor de Nível
Instrução de trabalho.

## Problema encontrado

Leitura do nível compensado, situado na estação Chapadinha Aviário DF108, não condiz com o valor da régua no local onde foi identificado sujeira de barro sobre o sensor dentro do cano que acomoda o sensor.

## Objetivo

Limpeza do Sensor de Nível Hidrostático situado na estação Chapadinha Aviário DF108 para correção da leitura de pressão.

## Pré-requisitos

1. Abraçadeira Elástica
1. Balde de água
1. Telefone Celular

## Procedimento in loco

> Os procedimentos serão acompanhados remotamente de um funcionário da ATIVA Soluções.

### Limpeza do Sensor

1. Acesse a caixa de passagem.
1. Tirar foto da caixa de passagem.
1. Puxar de forma suave o sensor até sair pela caixa de passagem.
1. Realizar a limpeza do sensor com água até que não exista resquícios de sujeira.
1. Tirar foto do sensor de nível.
1. Com o balde de água jogar a água na tubulação pela caixa de passagem afim de limpar a tubulação de sujeiras.
1. Verificar na saída da tubulação se existe sujeira acumulada e realizar a limpeza, caso haja, repetir a ação acima.

> Entrar em contato com o funcionário da ATIVA para realizar a leitura/validação do sensor fora da água.
{.is-warning}

### Reinstalação do sensor

1. Colocar novamente o sensor na tubulação e deslizar até chegar no ponto da amarração anterior.
1. Tirar foto da caixa de passagem.
1. Tirar foto da régua.

> Entrar em contato com o funcionário da ATIVA para realizar a leitura do sensor com o sensor dentro da água.
{.is-warning}