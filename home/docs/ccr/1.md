---
title: CCR Doc 1
description: 
published: true
date: 2020-05-28T13:10:29.731Z
tags: 
editor: undefined
---

![Logo ATIVA](https://www.ativasolucoes.com.br//images/Logo-ATIVA.png =100x)![](https://s3-sa-east-1.amazonaws.com/prod-jobsite-files.kenoby.com/uploads/grupoccr-1541593344-ccrjpg.jpg =100x)

---
# Migração da infra do servidor dabatase
## Objetivo
Migrar o servidor database mongo, clonando o servidor para outra infraestrutura, visando o menor impacto ao usuário.

## Pré-requisitos
- IP/URL da nova infraestruta para acesso ao servidor database.
- Acesso SSH ao servidor de aplicação.
## Procedimento
1. Acessar via SSH o servidor de aplicação.
1. Parar o serviço backend.
1. Alterar host no properties do arquivo jar backend.
1. Iniciar o serviço do backend.
1. Validar aplicação e conexão.

## Impacto do procedimento
> Os equipamentos nos Shuttles enviam periodicamente dados instatâneos ao servidor backend, após o envio dos dados o backend registra os dados no database.

- Conforme informação acima, por se tratar apenas de dados instatâneos não temos geração de logs, portanto durante o periodo de migração ao novo servidor não teremos dados para povoar o database, logo teremos lacunas vazias no período transitório.

### Quantitativo
- 20 minutos para realização do procedimento e validações.

# Mudança de IP e utilização de URL
## Objetivo
Alterar o endereçamento da aplicação para URL, onde atualmente utilizamos endereços IP.

> Com a futura alteração do link foi previsto, pelo GRUPO CCR, a alteração do IP dos servidores, e identificado, pela ATIVA, a necessidade de reconfiguração do frontend, por sugestão começaremos a utilizar URL para endereçamento.

### Impactos positivos
Com as mesmas vantagens do DDNS, caso haja mudanças no IP do servidor, ao utilizarmos URL não temos necessidade de reconfigurar os endereçamentos visto que a URL será atualizada para o novo endereço IP de forma automática.

## Pré-requisitos
- IP/URL da nova infraestruta para acesso ao servidor de aplicação.
- Acesso SSH ao servidor de aplicação.

## Procedimento
1. Acessar via SSH o servidor de aplicação.
1. Parar o serviço frontend.
1. Substituir frontend pelo projeto previamente compilado.
1. Iniciar o serviço frontend.
1. Validar aplicação e conexão.

## Impacto do procedimento
- Indisponibilidade temporaria do site supervisório até que o serviço do frontend seja novamente inciado.

### Quantitativo
- 20 minutos para realização do procedimento e validações.