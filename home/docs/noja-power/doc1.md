---
title: Doc1
description: 
published: true
date: 2020-07-29T13:54:13.125Z
tags: 
editor: undefined
---

```mermaid
sequenceDiagram
participant SCADA as SCADA (Client)
participant SrvC as Servidor de Conexões (Server)
participant Marthe as Marthe (Client)
  autonumber
	SCADA->>+SrvC: Request TCP Connection
  SrvC-->>-SCADA: Accept Connection
  Marthe->>+SrvC: Request TCP Connection
  SrvC-->>-Marthe: Accept Connection
```

```mermaid
sequenceDiagram
participant SCADA as SCADA [Master]
participant SrvC as Servidor de Conexões
participant Marthe as Marthe
participant RM as Relay Module (NOJA Power) [Slave]
  autonumber
	SCADA->>SrvC: Query message
  SrvC->>Marthe: Query Message
  Marthe->>RM: Query Message
  RM-->>Marthe: Response Message
  Marthe-->>SrvC: Response Message
  SrvC-->>SCADA: Response Message
```