---
title: Controle de pressão de Saída da VRP não-conforme
description: 
published: true
date: 2020-07-18T20:47:40.249Z
tags: 
editor: undefined
---

![Logo ATIVA](https://www.ativasolucoes.com.br//images/Logo-ATIVA.png =100x)

> Documento sem revisão!
{.is-danger}

> Raphael França
> CAAC
> 18/07/2020
{.is-info}

# Laudo
Controle de pressão de Saída da VRP não-conforme
# Situação problema
Os valores de vazão da VRP Cidade Jardim estão com faixa muito grande, fora do admissivel, no período noturno onde o valor deveria se manter em 10mca de acordo com a configuração, segue imagens abaixo:
![grafico_pressao_saida_vrp_saecil.png](/images/saecil/grafico_pressao_saida_vrp_saecil.png){.align-center}
https://homologacao.ativasolucoes.com.br/
u: saecil
s: saecil
Equip: VRP (021120)

## Decorrência do problema
Cliente informou que esta variação muito grande de pressão implica em vazamento nas ruas, e pode acarretar na danificação das tubulações.

# Diagnóstico
O período atual de leitura dos dados da IO estavam em 5minutos, portanto, conforme sensibilidade configurada para atuar aguarda período de 5minutos, podemos identificar no gráfico que após 5minutos a VRP controla a pressão de saída para o configurado.
Com esta análise deduzimos que no local quando a pressão de saída é baixa no período noturno esta tem uma variação muito grande não prevista, portanto devemos ajustar o sistema mais sensível.

# Ações para correção/mitigação
Após conversas com o Jean, decidimos tomar as seguintes ações:
1. Diminuir o período de leitura da IO4 (EA Pressão Saída)
	- O resultado desta ação é que a controladora VRP consegue detectar mais rapidamente que o valor da pressão de saída ultrapassou os limites de pressão configurados, e vai atuar para reestabelecer o valor de 10mca de pressão.
1. Retirar os setpoints estabelecidos no período noturno (00:00-05:00)
	- O resultado desta ação resulta em que o equipamento se mantêm no setpoint pressão 23mca definido como base, com isso podemos identificar se esta ação resulta em uma melhor sintonia, onde o esperado seria um gráfico mais liso, com uma faixa menor de limites.

# Resultado das ações aplicadas
O equipamento conecta no supervisório a cada 4horas para transmitir os dados. Modifiquei a rota da conexão 45004 para o servidor 10.10.10.252, onde está o servcon, com isso foi possível conectar no equipamento às 16:00 e realizar as ações definidas.

> Alterado valor do tempo de máximo de conexão para tentativa de transmissão para 60minutos com sucesso.
> Porém mesmo após alterar o valor, e confirmar a mudança, o equipamento parou de bater após 8minutos.
{.is-warning}

> Alterado o valor do intervalo da IO4 para 1minuto com sucesso.
{.is-success}

> Desabilitar período noturno com setpoint para 10mca sem sucesso.
{.is-danger}

## Analisando a impossibilidade de alterar o setpoint noturno
Ao tentar realizar a mudança do período do Set Point 1 e Set Point 2 referentes aos horário noturno com pressão 10mca, o sirius enviava o pacote appconfig porém não recebia mais retorno do equipamento.

Possíveis problemas admitidos:
1. Conexão rede-móvel com sinal pobre
`Plausível`
Ao realizar a configuração dos outros 2 procedimentos, o envio foi feito com sucesso na primeira tentativa, e ao tentar baixar as informações via sirius para confirmar a mudança foi feito com sucesso na primeira tentativa.
A mesma situação ocorreu com o Jean, o qual me reportou que ao tentar enviar as configurações do setpoint não tinha sucesso no envio.

1. Problema no Sírius configurador
`Plausível`
Foi realizado o envio incremental das mudanças, portanto, primeiro alterei o periodo de tentativa máximo de conexão, depois o tempo de logs da IO4, e só após tentei realizar o envio onde desabilito setpoint noturno. Tentei realizar duas vezes o envio porém ambos os pacotes saiam porém não retornavam.
É conhecido que o Siríus não é identico para conexão serial e tcp, portanto ambas devem ser desenvolvidas para um novo firmware, logo podemos admitir que temos problemas no envio via TCP do setpoint noturno.

# Ações a serem tomadas
1. Monitoramento da VRP
Como não foi possível alterar os valores do setpoint noturno, o equipamento ainda continuará abaixando o valor para 10mca a noite, todavia devemos observar se apenas com a mudança do valor de tempo de leitura da IO4 conseguirá estabilizar o valor da pressão de saída na faíxa admissivel pela SAECIL.

1. Estudar outras possibilidades de parametrização
A fim de refinar a estabilidade nos valores de pressão de saída.

# Informativo
> O cliente Giuliano Maia da SAECIL tem ciência de todas as ações tomadas.
{.is-info}
