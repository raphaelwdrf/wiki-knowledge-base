# Linux
None

## Gitlab Cheat Sheet
https://docs.gitlab.com/ee/administration/troubleshooting/linux_cheat_sheet.html

## Inspect permissions on folder/file
```powershell
stat -c '%a %n' file_path/file
```
```powershell
$ stat -c '%a %n' /etc/conf.d/staticroute
755 /etc/conf.d/staticroute
```
`755` on symbolic notation `-rwxr-xr-x`
> Good to know
> https://chmodcommand.com/
## Inspect OS version
```powershell
cat /etc/os-release
```
```powershell
$ cat /etc/os-release
NAME="Ubuntu"
VERSION="18.04.4 LTS (Bionic Beaver)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 18.04.4 LTS"
VERSION_ID="18.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic
```

## Find
```powershell
find / -iname 'exampleName'
find /home/username/ -iname '*.err'
find . -iname '*exampleName*' # best way to use

locate exampleName
whereis exampleName
which exampleName
```
> `Find` is the only command, when using parameter 'iname', that is insensitive(not case-sensitive).
{.is-warning}

### Source
https://www.linode.com/docs/tools-reference/tools/find-files-in-linux-using-the-command-line/

## TCPDump
```powershell
tcpdump -i InterfaceName -n Expression
```
```powershell
$ ifconfig # To discover interfacename
ens160: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500 # ens160 is our interfacename
        inet 10.10.10.240  netmask 255.255.255.0  broadcast 10.10.10.255
        inet6 fe80::20c:29ff:fe58:dbf2  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:58:db:f2  txqueuelen 1000  (Ethernet)
        RX packets 322574  bytes 128127152 (128.1 MB)
        RX errors 0  dropped 253935  overruns 0  frame 0
        TX packets 28151  bytes 3940599 (3.9 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

$ sudo tcpdump -i ens160 -n port 31000 # Filter on port 31000
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on ens160, link-type EN10MB (Ethernet), capture size 262144 bytes
14:35:22.333660 IP 177.97.201.43.2745 > 10.10.10.240.31000: Flags [S], seq 2853753341, win 384, options [mss 536], length 0
14:35:22.333739 IP 10.10.10.240.31000 > 177.97.201.43.2745: Flags [R.], seq 0, ack 2853753342, win 0, length 0
^C # Sent 'Ctrl+C' to end capture
2 packets captured
2 packets received by filter
0 packets dropped by kernel
```

### Source
https://www.dicas-l.com.br/arquivo/como_utilizar_o_tcpdump.php

## Opening GUI Software remotely via SSH/Shell
```powershell
export DISPLAY=:0 # choose display to open gui (defining variable only to running shell)
xhost + # Need to run at least once on machine
```
```powershell
$ export DISPLAY=:0 
$ xhost + 
$ firefox www.google.com.br
```
> Simple explanation about display
> https://stackoverflow.com/a/1210332
> {.is-info}
### Source 
https://askubuntu.com/a/417112
https://askubuntu.com/questions/417111/open-firefox-from-bash-file-error-no-display-specified#comment539969_417112

## Disable swappiness
## {.tabset}
### Ubuntu

> Pertinent:
> This is a less intrusive way than [#disable-swap](#disable-swap), but maybe it does not work as [here](https://serverfault.com/questions/978780/remove-swap-completely-or-set-swappiness-to-0#comment1273746_978787).
{.is-info}

- Edit sysctl.conf
```powershell
nano /etc/sysctl.conf
```
- Insert on sysctl.conf
`vm.overcommit_memory=1`
`vm.swappiness=0`

- Reboot system and check values:
```powershell
sysctl -a | grep 'vm.swappiness\|vm.overcommit_memory'
```
```powershell
$ sysctl -a | grep 'vm.swappiness\|vm.overcommit_memory'
vm.overcommit_memory = 1
vm.swappiness = 0
```
> Do not use sudo to check variables or it will take a long time to process.
{.is-warning}

#### Source
https://success.docker.com/article/node-using-swap-memory-instead-of-host-memory

## Disable swap
## {.tabset}
### Ubuntu
```powershell
swapon --show # Show swap images enabled
swapoff -v /swapfile # deactivate the swap space
vi /etc/fstab # Remove the entry /swapfile swap swap defaults 0 0
rm /swapfile # Remove swap image file
reboot now # Reboot for changes in order to have effect
```

```powershell
$ sudo swapon --show
NAME      TYPE SIZE USED PRIO
/swap.img file 1.9G   1M   -2

$ sudo swapoff -v /swap.img
swapoff /swap.img

$ sudo nano /etc/fstab

$ sudo rm /swap.img

$ sudo reboot 0
```

#### Source
https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-18-04/

## Set timezone
## {.tabset}
### Alpine
```powershell
apk add tzdata
ls /usr/share/zoneinfo
cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
echo "America/Sao_Paulo" > /etc/timezone
date
apk del tzdata
```
#### Source
https://wiki.alpinelinux.org/wiki/Setting_the_timezone

## Crontab
> Remember crontab is attached to the user running the shell, it will run with the privileges of that user account.
> As default always edit /etc/crontab.
> So as default run as root (sudo).
{.is-warning}
- Edit crontab
```powershell
sudo nano /etc/crontab # crontab systemwide crontab
```
```powershell
0 5 * * * root /sbin/shutdown -r now
```
- Show crontab
```powershell
sudo crontab -l
```

### Source
https://askubuntu.com/questions/609850/what-is-the-correct-way-to-edit-a-crontab-file

## Terminal Output

> Pertinent:
> Exhaustive instructions to output
> http://www.learnlinux.org.za/courses/build/shell-scripting/ch01s04.html
> https://www.howtogeek.com/435903/what-are-stdin-stdout-and-stderr-on-linux/
{.is-info}

```powershell
          || visible in terminal ||   visible in file   || existing
  Syntax  ||  StdOut  |  StdErr  ||  StdOut  |  StdErr  ||   file   
==========++==========+==========++==========+==========++===========
    >     ||    no    |   yes    ||   yes    |    no    || overwrite
    >>    ||    no    |   yes    ||   yes    |    no    ||  append
          ||          |          ||          |          ||
   2>     ||   yes    |    no    ||    no    |   yes    || overwrite
   2>>    ||   yes    |    no    ||    no    |   yes    ||  append
          ||          |          ||          |          ||
   &>     ||    no    |    no    ||   yes    |   yes    || overwrite
   &>>    ||    no    |    no    ||   yes    |   yes    ||  append
          ||          |          ||          |          ||
 | tee    ||   yes    |   yes    ||   yes    |    no    || overwrite
 | tee -a ||   yes    |   yes    ||   yes    |    no    ||  append
          ||          |          ||          |          ||
 n.e. (*) ||   yes    |   yes    ||    no    |   yes    || overwrite
 n.e. (*) ||   yes    |   yes    ||    no    |   yes    ||  append
          ||          |          ||          |          ||
|& tee    ||   yes    |   yes    ||   yes    |   yes    || overwrite
|& tee -a ||   yes    |   yes    ||   yes    |   yes    ||  append
```

### Explanation
### {.tabset}
#### >
`command > output.txt`

The standard output stream will be redirected to the file only, it will not be visible in the terminal. If the file already exists, it gets overwritten.

#### >>
`command >> output.txt`

The standard output stream will be redirected to the file only, it will not be visible in the terminal. If the file already exists, the new data will get appended to the end of the file.

#### 2>
`command 2> output.txt`

The standard error stream will be redirected to the file only, it will not be visible in the terminal. If the file already exists, it gets overwritten.

#### 2>>
`command 2>> output.txt`

The standard error stream will be redirected to the file only, it will not be visible in the terminal. If the file already exists, the new data will get appended to the end of the file.

#### &>
`command &> output.txt`

Both the standard output and standard error stream will be redirected to the file only, nothing will be visible in the terminal. If the file already exists, it gets overwritten.

#### &>>
`command &>> output.txt`

Both the standard output and standard error stream will be redirected to the file only, nothing will be visible in the terminal. If the file already exists, the new data will get appended to the end of the file..

#### | tee
`command | tee output.txt`

The standard output stream will be copied to the file, it will still be visible in the terminal. If the file already exists, it gets overwritten.

#### | tee -a
`command | tee -a output.txt`

The standard output stream will be copied to the file, it will still be visible in the terminal. If the file already exists, the new data will get appended to the end of the file.

#### (*)
`(*)`

Bash has no shorthand syntax that allows piping only StdErr to a second command, which would be needed here in combination with tee again to complete the table. If you really need something like that, please look at "How to pipe stderr, and not stdout?" on Stack Overflow for some ways how this can be done e.g. by swapping streams or using process substitution.

#### |& tee
`command |& tee output.txt`

Both the standard output and standard error streams will be copied to the file while still being visible in the terminal. If the file already exists, it gets overwritten.

#### |& tee -a
`command |& tee -a output.txt`

Both the standard output and standard error streams will be copied to the file while still being visible in the terminal. If the file already exists, the new data will get appended to the end of the file.

### Source
https://askubuntu.com/a/731237

## IP route
### Shell
> Not persistent
{.is-warning}
```powershell
ip route add $ip/netmask via $gateway dev $inetname
```
```powershell
$ ip route add 10.0.0.0/8 via 0.0.0.0 dev tun0
$ /sbin/route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
...
0.0.0.0         172.21.0.1      0.0.0.0         UG    0      0        0 eth0
1.1.1.1         0.0.0.0         255.255.255.255 UH    0      0        0 tun0
10.0.0.0        0.0.0.0         255.0.0.0       U     0      0        0 tun0
...
```
### Staticroute
> Persistent
{.is-warning}

> Not fully tested
{.is-danger}

- Edit/Create file
`/etc/conf.d/staticroute`

#### {.tabset}
##### Alpine
```powershell
# Define static routes on Linux using iproute2. See ip(8) for syntax.
staticiproute="192.168.0.0/24 via 10.73.1.1; 192.168.1.0/24 via 10.73.1.1"
```
```powershell
/etc/conf.d/staticroute start
rc-update add staticroute # set the staticroute service to start automatically on boot
reboot now # reboot to changes take effect
```
#### Details
```powershell
# Static routes are defined differently depending on your operating
# system, so please be sure to use the correct syntax.
# Do not use this file to define the default route.
# In all settings, multiple routes should be separated using ; or new lines.

# Define static routes on Linux using route. See route(8) for syntax.
#staticroute="net 192.168.0.0 netmask 255.255.255.0 gw 10.73.1.1
#net 192.168.1.0 netmask 255.255.255.0 gw 10.73.1.1"

# Define static routes on Linux using iproute2. See ip(8) for syntax.
#staticiproute="192.168.0.0/24 via 10.73.1.1; 192.168.1.0/24 via 10.73.1.1"

# Define static routes on GNU/Hurd. See route(8) for syntax.
# /etc/route.conf(5) takes precedence over this configuration.
# FIXME: "net ..." not supported
#staticroute="net 192.168.0.0 -netmask 255.255.255.0 --address 10.73.1.1
#net 192.168.1.0 -netmask 255.255.255.0 --address 10.73.1.1"

# Define static routes on GNU/KFreeBSD. See route(8) for syntax.
#staticroute="net 192.168.0.0 10.73.1.1 netmask 255.255.255.0
#net 192.168.1.0 10.73.1.1 netmask 255.255.255.0"

# Define static routes on other BSD systems. See route(8) for syntax.
# /etc/route.conf(5) takes precedence over this configuration.
#staticroute="net 192.168.0.0 -netmask 255.255.255.0 10.73.1.1
#net 192.168.1.0 -netmask 255.255.255.0 10.73.1.1"
```

### Scriptable
> MEN AT WORK
{.is-danger}

- Folders to create script
`/etc/network/*`

### Check route
```powershell
ip route get $ip
```
```powershell
$ ip route get 10.155.249.18
10.155.249.18 dev tun0 src 10.7.236.137 uid 0
    cache
```
### Source
https://wiki.alpinelinux.org/wiki/How_to_configure_static_routes
https://github.com/OpenRC/openrc/blob/master/conf.d/staticroute
https://unix.stackexchange.com/questions/486834/how-to-persist-iproute2-routes-and-rules-in-alpine-linux

## Kill TCP Connection
- Find the offending process: `netstat -np`
- Find the socket file descriptor: `lsof -np $PID`
- Debug the process: `gdb -p $PID`
- Close the socket: `call close($FD)`
- Close the debugger: `quit`

## Open listen port netcat
https://www.journaldev.com/34113/opening-a-port-on-linux

`nc -l -p 4000`
On link there is a full-detailed tutorial about this procedure.

## Deactivate GUI on Ubuntu Desktop
Como remover ou desabilitar a interface gráfica no Linux Ubuntu?
ELIGO.FAQ-52111
Para remover ou desabilitar o ambiente gráfico de um SO Linux Ubuntu deve-se seguir os procedimentos a seguir:
1. Abrir o terminal (deve-se estar logado como root ou utilizar o comando sudo su para adquirir direitos de administrador do ambiente).
2. Digitar a linha de comando:
nano /etc/default/grub
3. Substituir a expressão quiet splash por text na linha GRUB_CMDLINE_LINUX_DEFAULT="quiet splash". Isso fará com que o sistema inicialize em modo texto, otimizando o desempenho do equipamento.
4. Digitar a linha de comando:
sudo update-grub

Para habilitar ou reabilitar a interface gráfica deve-se executar o procedimento anterior, substituindo a expressão text por quiet splash na linha GRUB_CMDLINE_LINUX_DEFAULT="text".

Para remover definitivamente a interface gráfica, deve-ser digitar os seguintes comados no terminal:
sudo apt-get remove ubuntu-desktop
sudo apt-get autoremove

### Source
http://ads.prd.inf.br:8080/eligo/man/faq-52111.html#:~:text=1.,direitos%20de%20administrador%20do%20ambiente).&text=3.,GRUB_CMDLINE_LINUX_DEFAULT%3D%22quiet%20splash%22