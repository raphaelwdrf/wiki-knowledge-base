---
title: Service
description: 
published: true
date: 2020-05-29T19:03:40.196Z
tags: 
editor: undefined
---

# Service
None

# Controlling service
```Shell
# Control whether service loads on boot
sudo systemctl enable my_service
sudo systemctl disable my_service

# Manual start and stop
sudo systemctl start my_service
sudo systemctl stop my_service

# Restarting/reloading
sudo systemctl daemon-reload # Run if .service file has changed
sudo systemctl restart my_restart

# Or if working with a user service add --user flag
systemctl --user restart my_user_service
```
## Source
https://www.devdungeon.com/content/creating-systemd-service-files

# Monitoring service
```Shell
# See if running, uptime, view latest logs
sudo systemctl status
sudo systemctl status my_service
# Or for a user service
systemctl --user status my_service

# See all systemd logs
sudo journalctl

# Tail logs
sudo journalctl -f

# Show logs for specific service
sudo journalctl -u my_daemon
```

## Source
https://www.devdungeon.com/content/creating-systemd-service-files

# How to create service on Linux Systemd
## Simple usage
- Create Service file (my_service.service)
```shell
[Unit]
Description=My_service systemd service.
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
WorkingDirectory=/usr/bin
ExecStart=/bin/bash /usr/bin/test_service.sh

[Install]
WantedBy=multi-user.target
```
- Move file to system folder
```Shell
sudo cp my_service.service /etc/systemd/system
```
- Enable service on boot
```Shell
# Control whether service loads on boot
sudo systemctl enable my_service
sudo systemctl disable my_service
```

- Start service and check status
```Shell
sudo systemctl start my_service
sudo systemctl status my_service
```
On status, it should appear 'Loaded' and 'Active', example below:
```
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2020-04-30 17:00:19 UTC; 4 days ago
     Docs: https://docs.docker.com
 Main PID: 1142 (dockerd)
```
If not, go to [controlling-monitoring-service](#controlling-service) to view logs and debug.

- Reboot system.
```Shell
sudo reboot 0
```

## Extended usage
- If you need detailed example with more functions.
```shell
[Unit]
# The [Unit] section of a .service file contains the description of the unit itself, and information about its
# behavior and its dependencies: (to work correctly a service can depend on another one).
Description=Example systemd service.
# [Description], description of the unit
After=network.target remote-fs.target nss-lookup.target httpd-init.service
# [After], State that our unit should be started after the units we provide in the form of a space-separated list.
Requires=avahi-daemon.socket
# [Requires], those are called hard dependencies.
# Unit (a service in our case) can depend on other units (not necessarily "service" units) to work correctly
# If any of the units on which a service depends fails to start, the activation of the service it's stopped
Wants=docker-storage-setup.service
# [Wants], list "soft" dependencies
# a failure of any "soft" dependency, however, doesn't influence what happens to the dependent unit.
StartLimitIntervalSec=0
# [StartLimitIntervalSec], This way, systemd will attempt to restart your service forever, because it wont burst.
# When you configure Restart=always as we did, systemd gives up restarting your service if it fails to start more than 5 times within a 10 seconds interval. Forever.

[Service]
# [Service] section of a service unit, we can specify things as the command to be executed when the service is started,
# or the type of the service itself. Let's take a look at some of them.
Type=simple
# [Type], Systemd defines and distinguish between some different type of services depending on their expected behavior.
# =simple; the command declared in ExecStart is considered to be the main process/service.
# =forking; expected to fork and launch a child process, which will become the main process/service.
# The parent process it's expected to die once the startup process is over.
# =oneshot; It works pretty much like simple: the difference is that the process is expected to finish its job before other units are launched.
# The unit, however, it's still considered as "active" even after the command exits, if the RemainAfterExit option is set to "yes" (the default is "no").
# =dbus; works like the simple type. Consequent units, however, are launched only after the DBus name is acquired.
# =notify; the daemon is expected to send a notification via the sd_notify function.
Restart=always
# Other restart options: always, on-abort, on-failure, etc
RestartSec=1
#at least 1 second, to avoid putting too much stress on your server when things start going wrong.
TimeoutStartSec=10
TimeoutStopSec=10
# Respectively, the timeout for a service startup and stop, in seconds.
# In the first case, if after the specified timeout the daemon startup process it's not completed, it will be considered to be failed.
ExecStart=/bin/bash /usr/bin/ff_kioskgrafana.sh
# [ExecStart]; [ExecStartPre]; [ExecStartPost]; [ExecStop]; [ExecReload]; [ExecStartPost]; [ExecStopPost]

[Install]
# The [install] section is needed to use
# `systemctl enable` to start on boot
# For a user service that you want to enable
# and start automatically, use `default.target`
# For system level services, use `multi-user.target`
WantedBy=multi-user.target
```

## Source
https://www.linode.com/docs/quick-answers/linux/start-service-at-boot/
https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
https://linuxconfig.org/how-to-create-systemd-service-unit-in-linux
https://www.devdungeon.com/content/creating-systemd-service-files