---
title: Useful
description: 
published: true
date: 2020-06-26T17:29:49.888Z
tags: 
editor: undefined
---

# First things to do after clean install

- Fish
```sh
apt install fish
```
- Nano syntax highlighting
```sh
git clone -b v2.9 https://github.com/scopatz/nanorc.git
mv nanorc/*.nanorc /usr/share/nano
echo "include /usr/share/nano/*.nanorc" >> /etc/nanorc # Sometimes already included
```

# Useful CLI software

## A must
- [tldr *The tldr-pages project is a collection help pages for command-line tools.*](https://github.com/tldr-pages/tldr)
{.links-list}

## Good to know
- [ncdu *Ncdu is a disk usage analyzer with an ncurses interface.*](https://dev.yorhel.nl/ncdu)
{.links-list}

# Study about linux

## Linux Ubuntu AVAIABLE and FREE explication
https://www.linuxatemyram.com/
