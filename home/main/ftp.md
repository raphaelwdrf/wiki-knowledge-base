---
title: FTP
description: 
published: true
date: 2020-05-14T20:30:22.894Z
tags: 
editor: undefined
---

# FTP
None

# Explicação FTP Passivo e Ativo

> O que muitas pessoas não sabem é que o Protocolo de Transferência de Arquivo (FTP) tem modos múltiplos de operação que podem afetar sua operação dramaticamente e, como resultado, a segurança de sua rede. Estes modos de operação determinam se o servidor de FTP ou o cliente de FTP inicia as conexões de TCP que são usadas para enviar informação do servidor para o cliente.
{.is-info}

O protocolo FTP suporta a operação dos dois modos, como segue:

- O primeiro modo de operação de FTP é conhecido como normal, entretanto é freqüentemente chamado ativo. Este modo de operação é tipicamente o padrão.

- O segundo modo de FTP de operação é conhecido como passivo.

No FTP ativo (normal), o cliente abre uma conexão de controle na porta 21 para o servidor, e sempre que o cliente pede dados do servidor, o servidor abre uma sessão de TCP na porta 20. No FTP passivo, o cliente abre as sessões de dados e usa um número de porta provida pelo servidor.

## Operação de FTP ativo

> O modo de operação ativo é menos seguro que o modo passivo. Este modo de operação complica a construção de firewalls, porque o firewall têm que prever que haverá uma conexão do servidor para o cliente pela porta 20. Os passos deste modo de operação são discutidos abaixo e mostrados na Figura 1.
{.is-info}


- O cliente abre um canal de controle (porta 21) para o servidor e fala para o servidor o número da porta que deverá vir a resposta. Este número de porta é determinado randomicamente e é maior que 1023.

- O servidor recebe esta informação e envia para o cliente um reconhecimento " ack ". O cliente e o servidor trocam comandos de controle nesta conexão.

- Quando o usuário pede para listar um diretório ou inicia enviando ou recebendo um arquivo, o software de cliente envia um " comando de PORTA " que inclui um número de porta maior que 1023 para o servidor usar na conexão de dados.

- O servidor então, abre uma conexão de dados na porta 20 para o cliente, através da " PORTA de comando".

- O cliente reconhece o fluxo de dados.

*Figura 1: Conexão de FTP no modo Ativo*
![ftp_ativo.jpg](/ftp/ftp_ativo.jpg){.align-center}

1. O Cliente de FTP abre um canal de comando para o servidor; fala para servidor o número de porta a ser usado.

1. Servidor de FTP reconhece

1. Servidor de FTP abre um canal de dados ao cliente na porta indicada.

1. O cliente reconhece o fluxos de dados.

## Operação de FTP passiva

> Este modo de operação é mais seguro porque todas as conexões estão sendo iniciadas do cliente, assim há menor chance de comprometer a conexão. A razão de ser chamado de passivo é que o servidor executa um " passive open". Os passos deste modo de operação é discutido abaixo e mostrado na Figura 2.
{.is-info}


- No FTP passivo, o cliente abre uma conexão de controle na porta 21 para o servidor, e então faz pedidos de modo passivo pelo uso do " comando de PASV ".

- O servidor concorda com este modo, e então seleciona um número de porta randomicamente (>1023), provendo este número de porta para o cliente fazer transferência de dados.

- O cliente recebe esta informação e abre um canal de dados para a porta do servidor-designado.

- O servidor recebe os dados e envia um " ack ".

*Figura 1: Conexão de FTP no modo Passivo*
![ftp_passivo.jpg](/ftp/ftp_passivo.jpg){.align-center}

1. o Cliente de FTP abre um canal de comando ao Servidor de FTP e requisita o " modo passivo "

1. Servidor de FTP aloca porta para o canal de dados e envia o número da porta à ser usada para transmissão de dados

1. O cliente de FTP abre o canal de dados na porta especificada

1. Servidor de FTP responde com um "ack" e começa a transmitir dados.

## Source
http://gtrh.tche.br/~berthold/etcom/redes2-2000/trabalhos/FTP_EltonMarques.htm