---
title: Git
description: 
published: true
date: 2020-06-10T20:05:24.688Z
tags: 
editor: undefined
---

# Git
None

## Clone specific branch
```powershell
git clone -b BranchName https://github.com/username/project.git
```

## Choose specific tag
- First clone git
```powershell
git clone https://github.com/username/project.git
```
- List tags if needed
```powershell
git tag -l
```
- Choose tag
```powershell
git checkout tags/<tag_name>
```

### Source
https://stackoverflow.com/a/792027