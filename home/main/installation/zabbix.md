---
title: Zabbix
description: 
published: true
date: 2020-05-21T16:57:00.516Z
tags: 
editor: undefined
---

# Zabbix
None

# Installation
System and software used to formulate this manual:
> System:
> - Ubuntu 18.04.4 LTS
> - Docker version 19.03.8
> - docker-compose version 1.17.1
> ---
> Container:
> - zabbix/zabbix-web-apache-mysql ubuntu-5.0-latest
> - zabbix/zabbix-agent2 alpine-5.0-latest
> - zabbix/zabbix-server-mysql ubuntu-5.0-latest
> - mariadb 10

Not a must to use the same versions, but beware from incompatibility

## First-Steps
- Install all containers using docker-compose
- Set charset utf8m4 on database

> Pertinent:
> [/en/home/database/mariadb#myini](/en/home/database/mariadb#myini)
{.is-info}

If not defined on yml utf8mb4 on mariadb parameter, you must set on my.ini parameters described below, before creating mariadb container.
```ini
[client]
...
default-character-set=utf8mb4
...
[mysql]
...
default-character-set=utf8mb4
...
[mysqld]
...
collation-server = utf8mb4_unicode_ci
init-connect='SET NAMES utf8mb4'
character-set-server = utf8mb4
...
```
- Change rw permissions on docker.sock
```shell
chmod 666 /var/run/docker.sock
```
> Need to learn how to put container user on system group 'docker'  so it dsn't need 666 only 660(public cant rwe (read write execute).
{.is-warning}

## Usage
Zabbix-Agent2 can work on mode passively or actively, identify the most suited for your solution.

- Passive:
Zabbix-server will query for information about an item which is configured on the zabbix server.

- Active:
Zabbix-agent on the remote monitored host can collect information about the host and send it to the zabbix-server.

Image explaning:
![02-1024x176.png](/images/zabbix/02-1024x176.png)

> Pertinent:
> https://blog.zabbix.com/zabbix-agent-active-vs-passive/9207/
{.is-info}

> As default all templates work on mode 'passive'.
> To work on mode active you must to mass update all 'item' 'type' from 'template' from 'Zabbix Agent' to 'Zabbix Agent (Active)'.
{.is-info}
## Source
https://blog.zabbix.com/zabbix-agent-active-vs-passive/9207/
https://sites.google.com/site/wikirolanddelepper/systen-monitoring/difference-between-zabbix-agent-active-passive