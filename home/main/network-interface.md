---
title: Network Interface
description: 
published: true
date: 2020-05-14T20:35:04.638Z
tags: 
editor: undefined
---

# Network
None

# IP Adressess
## Internal local network
Special sets of numbers that are designed not to be assigned to any particular person. Instead, they are general allocations that are either used in special ways, or designed for people to use internally within local networks.

These numbers are primarily in the following ranges:

1. Begins with 10. (i.e. 10.0.0.0 through to 10.255.255.255)
1. Begins with 127.
1. Begins with 169.254.
1. Begins with 172.16. through 172.31.
1. Begins with 192.168.

## Source
https://www.iana.org/abuse

# Port
Port numbers are assigned in various ways, based on three ranges:
- The System Ports, also known as the Well Known Ports, from 0-1023
- The User Ports, also known as the Registered Ports, from 1024-49151
- The Dynamic Ports, also known as the Private or Ephemeral Ports, from 49152-65535

> - System ports are reserved ports used from the system.
> - User ports are ports to use own your protocol/application.
> - Dynamic ports are used from the system when it needs to choose a port randomly.
{.is-info}

> Must use only User Ports!
> Recommended to use high value ports, range from 30000-49000.
{.is-warning}

## Checking registry ports
https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml


## Source
https://www.iana.org/abuse
https://tools.ietf.org/html/rfc6056
https://www.iana.org/form/ports-services
https://tools.ietf.org/html/rfc6335
https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml