---
title: Python
description: 
published: true
date: 2020-06-05T14:06:12.517Z
tags: 
editor: undefined
---

# Python
None

# Environment variables

## Importing
```py
import os # import variables from os
os.environ['VARIABLE_NAME'] # call variable
```
```py
import os

SECRET_KEY = os.environ['SECRET_KEY']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DATABASE_NAME'],
        'HOST': os.environ['DATABASE_HOST'],
        'PORT': int(os.environ['DATABASE_PORT']),
    }
}
```
> You need to convert types manually (as you can check on DATABASE_PORT usage).
{.is-warning}

### Source
https://djangostars.com/blog/configuring-django-settings-best-practices/

## Defining variable
```py
os.environ['LD_LIBRARY_PATH'] = 'my_path'
```

- Example showing how to define a default value.
```py
import os

if "POSTGRES_DB" not in os.environ:
    os.environ['POSTGRES_DB'] = 'taiga'
        
'NAME': os.environ['POSTGRES_DB']
```
### Source
https://stackoverflow.com/a/15375701