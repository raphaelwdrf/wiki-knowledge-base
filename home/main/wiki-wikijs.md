---
title: WIki Wiki.js
description: 
published: true
date: 2020-05-26T17:28:24.330Z
tags: 
editor: undefined
---

# Wiki Wiki.js
None

# Data Converter
Softwares to convert data to markdown compatible with wiki.js
### Spreadsheet to markdown-table
https://tabletomarkdown.com/convert-spreadsheet-to-markdown/

# Break page
It does not affect visual on wiki, but it affects when priting PDF.
```html
<div style="page-break-after: always; break-after: page;"></div>
```

# Resize images
```
![](./pic/pic1_50.png =100x20)
```

## Source
https://stackoverflow.com/questions/14675913/changing-image-size-in-markdown

# Markdown Highligh protocols
https://github.com/github/linguist/blob/master/lib/linguist/languages.yml

## Source
https://stackoverflow.com/questions/20303826/highlight-bash-shell-code-in-markdown