---
title: Wireshark
description: 
published: true
date: 2020-05-14T20:37:53.349Z
tags: 
editor: undefined
---

# Wireshark
None

# Filters

## Display filters
https://packetlife.net/media/library/13/Wireshark_Display_Filters.pdf

## TCPDump
https://packetlife.net/media/library/12/tcpdump.pdf

## Dumpcap filters
https://www.wireshark.org/docs/man-pages/dumpcap.html

## Capture filters
https://www.wireshark.org/docs/man-pages/pcap-filter.html

# Dumpcap
Dumpcap gera logs sniffer utilizando o minimo de memória possível, com isso podemos utilizar o Dumpcap por tempo ilimitado, sua limitação será o espaço no disco.

## Utilização do Dumpcap
```Shell
cd "C:\Program Files\Wireshark" # Diretório onde se encontra o Dumpcap
dumpcap -d # Retorna as placas de redes existentes
dumpcap -i 1 -f "port 8080" # Selecionado a placa ID 1 com o Filtrando porta 8080
```

> Os filtros a serem utilizados estão expostos em [Dumpcap filters](http://10.10.10.213:7009/en/home/general/wireshark#dumpcap-filters).
{.is-info}


Exemplo de utilização:
```Shell
C:\Program Files\Wireshark>dumpcap -i 10 -f "port 80"
Capturing on 'Ethernet'
File: C:\Users\RAPHAE~1.FRA\AppData\Local\Temp\wireshark_Ethernet_20200508140236_a18692.pcapng
Packets captured: 61
Packets received/dropped on interface 'Ethernet': 61/0 (pcap:0/dumpcap:0/flushed:0/ps_ifdrop:0) (100.0%)
```

Para interromper a captura dos dados envie o comando 'CTRL+C' no terminal.

> O arquivo gerado deve ser interrompido por comando antes de aberto ou copiado, caso contrário os logs estarão fora de ordem e portanto não são dados válidos.
{.is-warning}