---
title: Pertinent
description: 
published: true
date: 2020-07-17T14:59:10.965Z
tags: 
editor: undefined
---

# Pertinent

# Information
https://www.journaldev.com/34113/opening-a-port-on-linux

The first 1024 ports (Ports 0-1023) are referred to as well-known port numbers and are reserved for the most commonly used services include SSH (port 22), HTTP and HTTPS (port 80 and 443), etc. Port numbers above 1024 are referred to as ephemeral ports.

Among ephemeral ports, Port numbers 1024-49151 are called the Registered/User Ports. The rest of the ports, 49152-65535 are called as Dynamic/Private Ports.

In this tutorial, we will show how we can open an ephemeral port on Linux, since the most common services use the well-known ports.

# Useful links
## Licenses
- [ChooseALicense *Licenses description*](https://choosealicense.com/licenses/)
- [ChooseALicense *Detailed table licenses description*](https://choosealicense.com/appendix/)
{.links-list}

## Scripts
### wait-for-it
https://github.com/vishnubob/wait-for-it

wait-for-it.sh is a pure bash script that will wait on the availability of a host and TCP port.

## Software
### Serial virtualization
https://sourceforge.net/projects/com0com/files/

```Files on /archive```
The Null-modem emulator is a kernel-mode virtual serial port driver for Windows.
aliases: com0com, com2tcp, hub4com.
