# E-mail

## Footer

### 1
Aviso Legal:
Esta mensagem é confidencial e dirigida apenas ao destinatário, e o seu emitente é responsável por seu conteúdo e endereçamento. Se a recebeu por erro solicitamos que comunique ao remetente e a elimine, assim como qualquer documento anexo. A transmissão incorreta desta mensagem não acarreta a perda de sua confidencialidade. É vedado a qualquer pessoa que não seja destinatário usar, revelar, distribuir ou copiar, ainda que parcialmente, esta mensagem.

Disclaimer:
This message is confidential and intended exclusively for the addressee, the issuer being responsible for its content and addressing. If you received it by mistake, please notify the sender and delete the message and any attachment. The incorrect transmission of this message does not entail the loss of its confidentiality. Any person other than the addressee is forbidden from using, disclosing, distributing or copying this message even partially.
