---
title: Main
description: 
published: true
date: 2020-05-14T20:38:50.604Z
tags: 
editor: undefined
---

# Main
None

# Batch
## Read and write output on terminal simultaneos
There are limitations using output file logs, one of them is that it's not possible to show in terminal and write output to a file simultaneos, so we need to use wtee software for the trick.

Download [wtee](https://github.com/stas-makutin/wtee) for windows.

---

Create batch file to run, example below:

`Language: batch`
`File format: .bat`

```Shell
@echo off
REM '@echo off' to hide output command on terminal view

set data=%date:~0,2%%date:~3,2%%date:~6,4%
set hora=%time:~0,2%%time:~3,2%%time:~6,2%
REM 'set data' and 'set hora' to set data and hour to output variable

"%appdata%\ATIVA Solucoes\Centaurus\jdk8u222-b10-jre\bin\java.exe" -jar centaurus.exe 2>&1 | wtee debug_output_%data%_%hora%.txt
REM run java.exe calling executable
REM '2>&1' combite stderr and stdout on output
REM run wtee in this situation to read and write output from java
```

## Source
https://github.com/stas-makutin/wtee
