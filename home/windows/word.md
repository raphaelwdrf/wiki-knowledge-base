# Variável referência
Selecione a palavra extenso "original". Clique na guia Inserir >> Links >> botão Indicador. Dê o nome Extenso e clique em Adicionar.
Clique numa parte remota do texto. Pressione Ctrl+F9 e escreva REF Extenso - de forma que o texto final fique { Ref Extenso } - e pressione a tecla F9.
Toda vez que você alterar o texto original do extenso, pressione Ctrl+T para selecionar todo corpo do documento e em seguida a tecla F9 para atualizar todos os campos.

## Source
https://social.msdn.microsoft.com/Forums/sqlserver/pt-BR/31fbf0a4-c836-40cb-82cd-3478de1885f6/word-2013-criar-variveis-para-mudar-automaticamente-a-cada-atualizao-em-um-contrato-extenso?forum=vbapt
